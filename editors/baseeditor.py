from PySide2.QtWidgets import QWidget
from PySide2.QtCore import Signal

from .editorIds import EditorIds
from .datatypes import AiBuilderSettings, BkProcessor


class BaseTabWidget(QWidget):

    RefreshAllTabs = Signal()

    def __init__(self, obj_settings, bkprocessor, bol_debug=False, parent=None):
        """

        :param obj_settings:
        :type obj_settings: AiBuilderSettings
        :param bkprocessor:
        :type bkprocessor: BkProcessor
        :param bol_debug:
        :type bol_debug:
        :param parent:
        :type parent:
        """
        super(BaseTabWidget, self).__init__(parent)
        self.Id = EditorIds.BaseTabId
        self.ProgressDialog = None
        self.Settings = obj_settings
        self.DebugFlag = bol_debug
        self.bkProcessor = bkprocessor

        self.bkProcessor.FileScanComplete.connect(self.reload)

    def toggleDebug(self):
        self.DebugFlag = not self.DebugFlag

    def reload(self):
        pass
