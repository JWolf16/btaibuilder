from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *
from ..datatypes import Selector


class TurnOrderWeightFactorModel(QAbstractTableModel):
    """
    selector table model

    """

    def __init__(self, parent=None):
        super(TurnOrderWeightFactorModel, self).__init__(parent)
        self.Header = ['Factor', 'Value']
        self.inventoryRecords = {}

    def update_table(self, dctFactors):
        self.beginResetModel()
        self.inventoryRecords = dctFactors
        self.endResetModel()
        return True
        # print 'Table Update Op took {0:02f} seconds'.format(time.time() - fl_st)

    def refresh(self):
        """
        refresh the table

        :return:
        :rtype:
        """
        self.beginResetModel()
        self.endResetModel()

    def rowCount(self, *args, **kwargs):
        return len(self.inventoryRecords)

    def columnCount(self, *args, **kwargs):
        return len(self.Header)

    def flags(self, index):
        """
        set flags for rows and columns in the table

        :param index: the index of the cell
        :type index:
        :return:
        :rtype:
        """
        if not index.isValid():
            return None
        if index.column() != 1:
            return super(TurnOrderWeightFactorModel, self).flags(index)
        else:
            # make the current value column editable
            flags = super(TurnOrderWeightFactorModel, self).flags(index)
            flags |= Qt.ItemIsEditable
            return flags

    def data(self, index, role):
        if not index.isValid():
            return None
        elif role not in [Qt.DisplayRole, Qt.EditRole]:
            return None
        record = sorted(self.inventoryRecords.keys())[index.row()]
        if index.column() == 0:
            return record
        elif index.column() == 1:
            return str(self.inventoryRecords[record])
        else:
            return None

    def setData(self, index, value, role):
        if not index.isValid():
            return False
        try:
            if index.column() == 1:
                record = sorted(self.inventoryRecords.keys())[index.row()]
                flVal = float(value)
                self.inventoryRecords[record] = flVal
                self.dataChanged.emit(index, index)
                return True
        except ValueError:
            return False

    def headerData(self, col, orientation, role):
        if orientation == Qt.Horizontal and role == Qt.DisplayRole:
            return self.Header[col]
        return None

    def deleteItems(self, lstItems):
        lst_remove = []
        for idx in lstItems:
            if idx.isValid():
                lst_remove.append(sorted(self.inventoryRecords.keys())[idx.row()])
        self.beginResetModel()
        for item in lst_remove:
            del self.inventoryRecords[item]
        self.endResetModel()

    def addItem(self, dtype, value):
        self.beginResetModel()
        self.inventoryRecords[dtype] = value
        self.endResetModel()
        return True
