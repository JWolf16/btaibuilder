from collections import OrderedDict


class BehaviourVariable(object):

    KeyField = 'k'
    ValField = 'v'
    TypeField = 'type'
    floatVal = 'floatVal'
    boolVal = 'boolVal'
    intVal = 'intVal'
    stringVal = 'stringVal'
    intType = 'Int'
    boolType = 'Bool'
    floatType = 'Float'
    stringType = 'String'


    def __init__(self):
        self.key = ''
        self.value = 0.0
        self.valType = self.floatVal


    def fromJson(self, data):
        """

        :param data:
        :type data: OrderedDict
        :return:
        :rtype:
        """
        self.key = data[self.KeyField]
        self.valType = data[self.ValField][self.TypeField]
        if self.valType == self.floatType:
            self.value = data[self.ValField][self.floatVal]
        elif self.valType == self.boolType:
            self.value = data[self.ValField][self.boolVal]
        elif self.valType == self.intType:
            # Seems to be a mistake but deal with it
            try:
                if self.intVal not in data[self.ValField]:
                    if self.floatVal in data[self.ValField]:
                        self.value = data[self.ValField][self.floatVal]
                    else:
                        self.value = data[self.ValField]['IntVal']
                else:
                    self.value = data[self.ValField][self.intVal]
            except:
                print(self.key)
                raise
        elif self.valType == self.stringType:
            self.value = data[self.ValField][self.stringVal]
        else:
            raise ValueError('Unknown Var Type: {0}'.format(self.valType))

    def toJson(self):
        data = OrderedDict()
        data[self.KeyField] = self.key
        data[self.ValField] = OrderedDict()
        data[self.ValField][self.TypeField] = self.valType
        if self.valType == self.floatType:
            data[self.ValField][self.floatVal] = self.value
        elif self.valType == self.boolType:
            data[self.ValField][self.boolVal] = self.value
        elif self.valType == self.intType:
            data[self.ValField][self.intVal] = self.value
        elif self.valType == self.stringType:
            data[self.ValField][self.stringVal] = self.value
        return data

    def fromOverrideJson(self, data, key):
        """

        :param data:
        :type data: OrderedDict
        :return:
        :rtype:
        """
        self.key = key
        self.valType = data[self.TypeField]
        if self.valType == self.floatType:
            self.value = data[self.floatVal]
        elif self.valType == self.boolType:
            self.value = data[self.boolVal]
        elif self.valType == self.intType:
            self.value = data[self.intVal]
        elif self.valType == self.stringType:
            self.value = data[self.stringVal]
        else:
            raise ValueError('Unknown Var Type: {0}'.format(self.valType))

    def toOverrideJson(self):
        data = OrderedDict()
        data[self.TypeField] = self.valType
        if self.valType == self.floatType:
            data[self.floatVal] = self.value
        elif self.valType == self.boolType:
            data[self.boolVal] = self.value
        elif self.valType == self.intType:
            data[self.intVal] = self.value
        elif self.valType == self.stringType:
            data[self.stringVal] = self.value
        return [data, self.key]
