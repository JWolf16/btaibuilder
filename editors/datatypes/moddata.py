from .teamaioverridedef import TeamAiOverrideDef
from .unitaioverridedef import UnitAiOverrideDef
from .behaviourvariable import BehaviourVariable
from .behaviournodeitem import RootNode


class ModData(object):

    def __init__(self):
        self.teamOverrides = [] # type: list[TeamAiOverrideDef]
        self.unitOverrides = [] # type: list[UnitAiOverrideDef]
        self.bVars = {} # type: dict[str, list[BehaviourVariable]]
        self.bTrees = [] # type: list[RootNode]

    def addTeamOverrides(self, override):
        """


        :param override:
        :type override: TeamAiOverrideDef
        :return:
        :rtype:
        """

        for item in self.teamOverrides:
            if item.Id == override.Id:
                return
        self.teamOverrides.append(override)

    @property
    def teamOverrideIds(self):
        ids = []
        for item in self.teamOverrides:
            ids.append(item.Id)
        return ids

    def getTeamOverride(self, id):
        for item in self.teamOverrides:
            if item.Id == id:
                return item
