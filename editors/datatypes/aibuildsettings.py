from .basexml import BaseXml


class AiBuilderSettings(BaseXml):

    XmlType = 'AiBuilderSettings'

    def __init__(self):
        self.BehaviourNodeDef = 'behaviour_nodedef.json'
        self.LastModPath = ''