from PySide2.QtCore import *
from PySide2.QtWidgets import *
from PySide2.QtGui import *

import sys
import json
import time
import os
import os.path
import pprint
from .forms import Ui_Form_UnitOverrideEdit
from .editorIds import EditorIds
from .baseeditor import BaseTabWidget
from .models import BehaviourVariableModel, BVarProxyModel, SelectorModel
from .datatypes import jsonReader, UnitAiOverrideDef
from .dialogs import NewBVarDialog, NewSelectorDialog
from .arrayeditor import GenericArrayEditor
import traceback


class UnitOverrideEditor(BaseTabWidget, Ui_Form_UnitOverrideEdit):

    """
    the inventory tab handler

    """

    def __init__(self, obj_settings, bkprocessor, bol_debug=False, parent=None):
        """

        :param obj_settings: the preference settings
        :type obj_settings:
        :param parent: the parent of this object (optional)
        :type parent: QObject
        """
        super(UnitOverrideEditor, self).__init__(obj_settings, bkprocessor, bol_debug, parent)
        self.setupUi(self)
        self.Id = EditorIds.UnitOverrideTabId
        self.CurrentOverrideDef = UnitAiOverrideDef()
        self.BVarDataModel = BehaviourVariableModel()
        self.selectorDataModel = SelectorModel()

        self.tableView_BVars.setModel(self.BVarDataModel)
        self.tableView_BVars.horizontalHeader().setSectionResizeMode(0, QHeaderView.Stretch)
        self.tableView_BVars.setSelectionMode(QAbstractItemView.ExtendedSelection)
        self.tableView_BVars.setContextMenuPolicy(Qt.ActionsContextMenu)

        actionRemove = QAction(self.tableView_BVars)
        actionRemove.setText('Remove Selected Item(s)')
        actionRemove.triggered.connect(self.handleBvarRemove)
        self.tableView_BVars.addAction(actionRemove)

        actionAdd = QAction(self.tableView_BVars)
        actionAdd.setText('Add New Behaviour Var')
        actionAdd.triggered.connect(self.newBVar)
        self.tableView_BVars.addAction(actionAdd)

        self.tableView_Selectors.setModel(self.selectorDataModel)
        self.tableView_Selectors.horizontalHeader().setSectionResizeMode(0, QHeaderView.Stretch)
        self.tableView_Selectors.horizontalHeader().setSectionResizeMode(1, QHeaderView.Stretch)
        self.tableView_Selectors.setSelectionMode(QAbstractItemView.ExtendedSelection)
        self.tableView_Selectors.setContextMenuPolicy(Qt.ActionsContextMenu)

        actionRemove = QAction(self.tableView_Selectors)
        actionRemove.setText('Remove Selected Item(s)')
        actionRemove.triggered.connect(self.handleSelectorRemove)
        self.tableView_Selectors.addAction(actionRemove)

        actionAdd = QAction(self.tableView_Selectors)
        actionAdd.setText('Add New Selector')
        actionAdd.triggered.connect(self.newSelector)
        self.tableView_Selectors.addAction(actionAdd)

        self.pushButton_LoadFile.pressed.connect(self.loadDef)
        self.pushButton_AddFactors.pressed.connect(self.editAddFactors)
        self.pushButton_RemoveFactors.pressed.connect(self.editRemoveFactors)
        self.pushButton_LoadBVars.pressed.connect(self.loadBvarFile)
        self.pushButton_Save.pressed.connect(self.saveDef)

        # self.pushButton_LoadFile.setVisible(False)

    def activateOverrideDef(self, overridedef):
        """

        :param overridedef:
        :type overridedef: UnitAiOverrideDef
        :return:
        :rtype:
        """

        self.CurrentOverrideDef = overridedef
        self.lineEdit_Id.setText(self.CurrentOverrideDef.Id)
        self.lineEdit_Ai.setText(self.CurrentOverrideDef.TreeRootName)
        self.spinBox_Priority.setValue(self.CurrentOverrideDef.Priority)
        self.BVarDataModel.changeBvars(self.CurrentOverrideDef.BehaviourVars)
        self.selectorDataModel.update_table(self.CurrentOverrideDef.Selectors)

    def saveDef(self):
        try:
            str_to_save = QFileDialog.getSaveFileName(self, caption="Save Unit AI Override Def", filter="Unit AI Override Def (*.json)", dir='./{0}.json'.format(self.lineEdit_Id.text()))[0]

            if str_to_save != '':
                self.CurrentOverrideDef.Id = self.lineEdit_Id.text()
                self.CurrentOverrideDef.TreeRootName = self.lineEdit_Ai.text()
                self.CurrentOverrideDef.Priority = self.spinBox_Priority.value()
                self.CurrentOverrideDef.Selectors = self.selectorDataModel.inventoryRecords
                self.CurrentOverrideDef.BehaviourVars = self.BVarDataModel.inventoryRecords
                mfile = open(str_to_save, 'w')
                data = self.CurrentOverrideDef.toJson()
                json.dump(data, mfile, indent=4)
                mfile.close()
                QMessageBox.information(self, 'Operation Success', 'Saved Unit AI Override Def')

        except Exception:
            print(traceback.format_exc())
            QMessageBox.critical(self, 'Operation Failed', 'Failed to Unit AI Override Def')

    def loadDef(self):
        try:
            str_file_to_load = QFileDialog.getOpenFileName(self, caption="Load UnitAiOverrideDef File",
                                                           filter="UnitAiOverrideDef(*.json)")[0]
            if str_file_to_load != '':
                data = jsonReader.readJsonFile(str_file_to_load)
                if data:
                    newDef = UnitAiOverrideDef()
                    newDef.fromJson(data)
                    self.activateOverrideDef(newDef)
                else:
                    raise Exception('Read Failure, check Json file is valid')
                QMessageBox.information(self, 'Operation Success', 'Loaded UnitAiOverrideDef')
        except Exception as e:
            print(traceback.format_exc())
            QMessageBox.critical(self, 'Operation Failed', str(e))

    def loadBvarFile(self):
        try:
            str_file_to_load = QFileDialog.getOpenFileName(self, caption="Load Behaviour Variable File",
                                                           filter="AI BehaviourVars (*.json)")[0]
            if str_file_to_load != '':
                data = jsonReader.readJsonFile(str_file_to_load)
                if data:
                    self.BVarDataModel.update_table(data)
                else:
                    raise Exception('Read Failure, check Json file is valid')
                QMessageBox.information(self, 'Operation Success', 'Loaded Behaviour Variables')
        except Exception as e:
            print(traceback.format_exc())
            QMessageBox.critical(self, 'Operation Failed', str(e))

    def handleBvarRemove(self):
        lst_idx = []
        for item in sorted(self.tableView_BVars.selectedIndexes()):
            # lst_idx.append(self.proxyModel.mapToSource(item))
            lst_idx.append(item)
        if len(lst_idx) > 0:
            self.BVarDataModel.deleteItems(lst_idx)

    def newBVar(self):
        dialog = NewBVarDialog()
        dialog.setWindowIcon(self.windowIcon())
        result = dialog.exec_()
        if result == QDialog.Accepted:
            ret = self.BVarDataModel.addItem(dialog.lineEdit_Name.text(), dialog.comboBox_Type.currentText(), dialog.lineEdit_Value.text())
            if not ret:
                QMessageBox.critical(self, 'Operation Failed', 'Value: {0}, is not of Type: {1}'.format(dialog.lineEdit_Value.text(), dialog.comboBox_Type.currentText()))

    def handleSelectorRemove(self):
        lst_idx = []
        for item in sorted(self.tableView_Selectors.selectedIndexes()):
            # lst_idx.append(self.proxyModel.mapToSource(item))
            lst_idx.append(item)
        if len(lst_idx) > 0:
            self.selectorDataModel.deleteItems(lst_idx)

    def newSelector(self):
        dialog = NewSelectorDialog()
        dialog.setWindowIcon(self.windowIcon())
        result = dialog.exec_()
        if result == QDialog.Accepted:
            self.selectorDataModel.addItem(dialog.lineEdit_Type.text(), dialog.lineEdit_Value.text())

    def editAddFactors(self):
        obj_dialog = GenericArrayEditor(self.CurrentOverrideDef.AddedInfluenceFactors, 'Add Factor', 'Remove Factor')
        obj_dialog.setWindowTitle('Edit Added Influence Factors')
        obj_dialog.setWindowIcon(self.windowIcon())
        obj_dialog.exec_()
        self.CurrentOverrideDef.AddedInfluenceFactors = obj_dialog.Tags

    def editRemoveFactors(self):
        obj_dialog = GenericArrayEditor(self.CurrentOverrideDef.RemovedInfluenceFactors, 'Add Factor', 'Remove Factor')
        obj_dialog.setWindowTitle('Edit Removed Influence Factors')
        obj_dialog.setWindowIcon(self.windowIcon())
        obj_dialog.exec_()
        self.CurrentOverrideDef.RemovedInfluenceFactors = obj_dialog.Tags