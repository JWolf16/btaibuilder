from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *

from ..forms import Ui_Dialog_newSelector
from ..datatypes import BehaviourVariable


class NewSelectorDialog(QDialog, Ui_Dialog_newSelector):

    """
    the dialog handler

    """

    def __init__(self, parent=None):
        """

        :param parent: the parent of this object (optional)
        :type parent: QObject
        """
        super(NewSelectorDialog, self).__init__(parent)
        self.setupUi(self)
        self.setWindowFlags(self.windowFlags() & ~Qt.WindowContextHelpButtonHint)