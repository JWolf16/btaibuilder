
class ENodeTypes(object):

    Undefined = 0
    Composite = 1
    Decorator = 2
    Leaf = 3
    Root = 4