from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *

from ..forms import Ui_Dialog_newBVar
from ..datatypes import BehaviourVariable


class NewBVarDialog(QDialog, Ui_Dialog_newBVar):

    """
    the generic array tab handler

    """

    def __init__(self, parent=None):
        """

        :param parent: the parent of this object (optional)
        :type parent: QObject
        """
        super(NewBVarDialog, self).__init__(parent)
        self.setupUi(self)
        self.setWindowFlags(self.windowFlags() & ~Qt.WindowContextHelpButtonHint)

        self.comboBox_Type.addItems([BehaviourVariable.floatType, BehaviourVariable.intType, BehaviourVariable.boolType, BehaviourVariable.stringType])

        self.comboBox_Type.setCurrentIndex(0)