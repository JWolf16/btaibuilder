from PySide2.QtCore import Qt, QSortFilterProxyModel


class GenericArrayProxyModel(QSortFilterProxyModel):

    def __init__(self, parent=None):
        QSortFilterProxyModel.__init__(self, parent)


    def lessThan(self, left, right):
        leftData = self.sourceModel().data(left, Qt.ItemDataRole)
        rightData = self.sourceModel().data(right, Qt.ItemDataRole)

        return leftData < rightData

    def filterAcceptsRow(self, sourceRow, sourceParent):
        return True