from PySide2.QtCore import *
from PySide2.QtWidgets import *

import sys
import json
import time
import os
import os.path
import pprint
from .forms import Ui_FormTreeBuilder
from .editorIds import EditorIds
from .baseeditor import BaseTabWidget
from .datatypes import CompositeNode, DecoratorNode, LeafNode, ENodeTypes
from .models import AiTreeModel
from .dialogs import NewNodeDialog
from collections import OrderedDict
import traceback


class TreeEditor(BaseTabWidget, Ui_FormTreeBuilder):

    """
    the inventory tab handler

    """

    def __init__(self, obj_settings, bkprocessor, bol_debug=False, parent=None):
        """

        :param obj_settings: the preference settings
        :type obj_settings:
        :param parent: the parent of this object (optional)
        :type parent: QObject
        """
        super(TreeEditor, self).__init__(obj_settings, bkprocessor, bol_debug, parent)
        self.setupUi(self)
        self.Id = EditorIds.AiTabId
        self.setWindowTitle('AI Tree Builder')
        self.dataModel = AiTreeModel()
        self.NodeDef = None
        if os.path.exists(self.Settings.BehaviourNodeDef):
            mfile = open(self.Settings.BehaviourNodeDef, 'r')
            self.NodeDef = json.load(mfile, object_hook=OrderedDict, object_pairs_hook=OrderedDict)
            mfile.close()
        else:
            str_file_to_load = QFileDialog.getOpenFileName(self, caption="Load NodeDef",
                                                           filter="NodeDef (*.json)")[0]
            if str_file_to_load != '':
                mfile = open(str_file_to_load, 'r')
                self.NodeDef = json.load(mfile, object_hook=OrderedDict, object_pairs_hook=OrderedDict)
                mfile.close()
            else:
                raise LookupError('Could not load Node Def')

        self.treeView.setModel(self.dataModel)
        # self.treeView.header().setResizeMode(QHeaderView.Stretch)
        self.pushButton_Load.pressed.connect(self.load_ai_tree)
        self.pushButton_Save.pressed.connect(self.save_tree)
        self.pushButton_New.pressed.connect(self.newTree)

        self.treeView.setContextMenuPolicy(Qt.CustomContextMenu)
        self.treeView.customContextMenuRequested.connect(self.openMenu)

        self.customMenu = QMenu(self.treeView)

        self.actionAdd = QAction(self.treeView)
        self.actionAdd.setText('New Node')
        self.actionAdd.triggered.connect(self.newNode)
        # self.customMenu.addAction(self.actionAdd)

        self.actionMove = QAction(self.treeView)
        self.actionMove.setText('Move Node')
        self.actionMove.triggered.connect(self.moveNode)

        self.actionDelete = QAction(self.treeView)
        self.actionDelete.setText('Delete Node')
        self.actionDelete.triggered.connect(self.deleteNode)
        # self.customMenu.addAction(self.actionDelete)

    def resizeEvent(self, event):
        """
        handle resize events so that the table stays nice

        :param event:
        :type event:
        :return:
        :rtype:
        """
        self.treeView.setColumnWidth(0, self.treeView.width()/2.5)

        QWidget.resizeEvent(self, event)

    def load_ai_tree(self):
        try:
            str_file_to_load = QFileDialog.getOpenFileName(self, caption="Load AI Tree",
                                                           filter="AI BehaviourTree (*.json)")[0]
            if str_file_to_load != '':
                unknownNodes = self.dataModel.loadTree(str_file_to_load, self.NodeDef)
                if len(unknownNodes) > 0:
                    str_msg = 'Unknown Nodes Found, add them as leaf nodes?\n'
                    if len(unknownNodes) < 20:
                        str_msg += '\n'.join(unknownNodes)
                    for x in range(0, len(unknownNodes), 2):
                        # if len(unknownNodes) > (x + 2):
                        #     str_msg += '\n{0}\t\t{1}\t\t{2}'.format(unknownNodes[x].ljust(longest), unknownNodes[x+1].ljust(longest), unknownNodes[x+2])
                        if len(unknownNodes) > (x + 1):
                            str_msg += '\n{0}{1}'.format(unknownNodes[x].ljust(75), unknownNodes[x + 1])
                        else:
                            str_msg += '\n{0}'.format(unknownNodes[x])
                    ret = QMessageBox.question(self, 'Add New Nodes\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t', str_msg, QMessageBox.Yes | QMessageBox.No)
                    if ret == QMessageBox.Yes:
                        for node in unknownNodes:
                            self.NodeDef['LeafNodes'].append(node)
                        mfile = open(self.Settings.BehaviourNodeDef, 'w')
                        json.dump(self.NodeDef, mfile, indent=4)
                        mfile.close()
                    else:
                        QMessageBox.critical(self, 'Operation Failed', 'Failed to load Tree, unknown nodes in use')
                        self.dataModel.resetData()
                        return

                self.treeView.expandAll()
                QMessageBox.information(self, 'Operation Success', 'Loaded AI Tree')
        except Exception as e:
            print(traceback.format_exc())
            QMessageBox.critical(self, 'Operation Failed', str(e))
            self.dataModel.resetData()

    def save_tree(self):
        try:
            if self.dataModel.isLoaded:
                str_to_save = QFileDialog.getSaveFileName(self, caption="Save AI Tree", filter="AI BehaviourTree (*.json)")[0]

                if str_to_save != '':
                    mfile = open(str_to_save, 'w')
                    data = self.dataModel.rootItem.toJson()
                    json.dump(data, mfile, indent=4)
                    mfile.close()
                    QMessageBox.information(self, 'Operation Success', 'Saved AI Tree')
            else:
                QMessageBox.warning(self, "Operation Failed", "Could not complete operation because no tree was loaded")
        except Exception:
            print(traceback.format_exc())
            QMessageBox.critical(self, 'Operation Failed', 'Failed to save Tree')

    def openMenu(self, pnt):

        lst_actions = []

        lst_idx = []
        for item in sorted(self.treeView.selectedIndexes()):
            lst_idx.append(item)
        if len(lst_idx) > 0:
            item = self.dataModel.getItem(lst_idx[0])
            if item == self.dataModel.rootItem:
                pass
            elif item.canHaveChildren:
                if item.childCount() < item.maxChildren:
                    lst_actions.append(self.actionAdd)
            if item != self.dataModel.rootItem:
                lst_actions.append(self.actionDelete)
                if item.parent().childCount() > 1:
                    lst_actions.append(self.actionMove)

        if len(lst_actions) > 0:
            QMenu.exec_(lst_actions, self.treeView.mapToGlobal(pnt))

    def newNode(self):
        lst_idx = []
        for item in sorted(self.treeView.selectedIndexes()):
            lst_idx.append(item)
        if len(lst_idx) > 0:
            item = self.dataModel.getItem(lst_idx[0])
            maxposx = item.childCount()
            dialog = NewNodeDialog(self.NodeDef, maxposx, "Add New Node")
            dialog.setWindowIcon(self.windowIcon())
            result = dialog.exec_()
            if result == QDialog.Accepted:
                nodetype = ENodeTypes.Composite
                if dialog.comboBox_Node.currentText() == 'Decorator Node':
                    nodetype = ENodeTypes.Decorator
                elif dialog.comboBox_Node.currentText() == 'Leaf Node':
                    nodetype = ENodeTypes.Leaf
                self.dataModel.insertNewNode(dialog.spinBox_Position.value(), nodetype, dialog.lineEdit_Name.text(), dialog.lineEdit_Type.text(), lst_idx[0])

    def deleteNode(self):
        lst_idx = []
        for item in sorted(self.treeView.selectedIndexes()):
            lst_idx.append(item)
        if len(lst_idx) > 0:
            self.dataModel.removeRow(lst_idx[0].row(), lst_idx[0].parent())

    def newTree(self):
        dialog = NewNodeDialog(self.NodeDef, 0, "New Tree Root", compositeOnly=True)
        dialog.setWindowIcon(self.windowIcon())
        result = dialog.exec_()
        if result == QDialog.Accepted:
            self.dataModel.newTree(dialog.lineEdit_Type.text(), dialog.lineEdit_Name.text())

    def moveNode(self):
        lst_idx = []
        for item in sorted(self.treeView.selectedIndexes()):
            lst_idx.append(item)
        if len(lst_idx) > 0:
            parent = self.dataModel.getItem(lst_idx[0].parent())
            output = QInputDialog.getInt(self, 'New Position', 'New Position', value=lst_idx[0].row(), maxValue=parent.childCount() -1, minValue=0)
            if output[1]:
                self.dataModel.moveItemPosition(lst_idx[0].parent(), lst_idx[0], output[0])