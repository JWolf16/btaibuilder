from PySide2.QtCore import QObject, Signal
from .moddata import ModData
import json
import os
import os.path
from .basejson import jsonReader
from .teamaioverridedef import TeamAiOverrideDef


class BkProcessor(QObject):

    StartFileScan = Signal(str)
    FileScanComplete = Signal(bool)


    def __init__(self, logger, parent=None):
        super(BkProcessor, self).__init__(parent)
        self.Logger = logger
        self.debugFlag = False

        self.CurrentMod = ModData()

        self.StartFileScan.connect(self._scanMod)

    def _scanMod(self, path):
        self.CurrentMod = ModData()
        bol_result = True
        if not path.endswith(os.sep):
            path += os.sep
        if os.path.exists(path + 'mod.json'):
            with open(path + 'mod.json') as f:
                try:
                    data = json.load(f)
                except:
                    self.Logger.error('Failed to read: {0}'.format(path + 'mod.json'))
                    raise
                bol_loaded = True
                try:
                    bol_loaded = data['Enabled']
                except KeyError:
                    bol_loaded = True
                if bol_loaded:
                    try:
                        lst_manifest = data['Manifest']
                        for entry in lst_manifest:
                            str_scan_path = entry['Path']
                            if str_scan_path.endswith(os.sep):
                                str_scan_path = str_scan_path.rstrip(os.sep)
                            if entry['Type'] in ['TeamAIOverrideDef']:
                                bol_result &= self._loadTeamOverrides(path + str_scan_path)

                    except KeyError:
                        pass
        self.FileScanComplete.emit(bol_result)

    def _loadTeamOverrides(self, path):
        try:
            if not os.path.exists(path):
                self.Logger.warning('Path: {0} does not exist...but is referenced by something, maybe this should be reported to someone'.format(path))
                return True
            lst_files = os.listdir(path)
            for item in lst_files:
                if os.path.isdir(path + os.sep + item):
                    if not self._loadTeamOverrides(path + os.sep + item):
                        return False
                    continue
                if item.lower().endswith('.json'):
                    jData = jsonReader.readJsonFile(path + os.sep + item)
                    if jData:
                        teamOverride = TeamAiOverrideDef()
                        teamOverride.fromJson(jData)
                        self.CurrentMod.addTeamOverrides(teamOverride)
                    else:
                        self.Logger.error("Failed to load Json File: {0}".format(item))

            return True
        except Exception as e:
            self.Logger.error("Unhandled Exception while loading Team AI Override Defs: {0}".format(e))
            return False
