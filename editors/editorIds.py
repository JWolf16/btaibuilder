
class EditorIds(object):
    BaseTabId = 'BaseTab'
    AiTabId = 'AI Tree'
    BVarTabId = 'Behaviour Variables'
    UnitOverrideTabId = 'Unit AI Override Def'
    TeamOverrideTabId = 'Team AI Override Def'