from .newnodedialog import NewNodeDialog
from .newbehaviourvardialog import NewBVarDialog
from .newselector import NewSelectorDialog