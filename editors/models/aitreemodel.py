from ..datatypes import CompositeNode, DecoratorNode, LeafNode, RootNode, ENodeTypes

from PySide2.QtCore import *
from PySide2.QtWidgets import *
from PySide2.QtGui import *

class AiTreeModel(QAbstractItemModel):

    def __init__(self):
        QAbstractItemModel.__init__(self)
        self.headers = ['Name', 'TypeName']
        self.rootItem = RootNode('Node Name', ' Node Type')
        self.isLoaded = False

    def resetData(self):
        self.beginResetModel()
        self.rootItem = RootNode('Node Name', ' Node Type')
        self.endResetModel()
        self.isLoaded = False

    def newTree(self, type, name):
        self.beginResetModel()
        self.rootItem = RootNode('Node Name', ' Node Type')
        self.rootItem.insertChild(name, type, ENodeTypes.Composite, 0)
        self.endResetModel()
        self.isLoaded = True

    def headerData(self, col, orientation, role=None):
        if orientation == Qt.Horizontal and role == Qt.DisplayRole:
            if col == 0:
                return self.rootItem.Name
            return self.rootItem.TypeName
        elif orientation == Qt.Vertical and role == Qt.DisplayRole:
            return col
        return None

    def rowCount(self, parent=None, *args, **kwargs):
        if parent.column() > 0:
            return 0

        if not parent.isValid():
            parentItem = self.rootItem
        else:
            parentItem = parent.internalPointer()

        return parentItem.childCount()

    def flags(self, index):
        if not index.isValid():
            return Qt.NoItemFlags
        if index.column() == 0:
            return Qt.ItemIsEnabled | Qt.ItemIsSelectable | Qt.ItemIsEditable
        return Qt.ItemIsEnabled | Qt.ItemIsSelectable

    def index(self, row, column, parent):
        if not self.hasIndex(row, column, parent):
            return QModelIndex()

        if not parent.isValid():
            parentItem = self.rootItem
        else:
            parentItem = parent.internalPointer()

        childItem = parentItem.child(row)
        if childItem:
            return self.createIndex(row, column, childItem)
        else:
            return QModelIndex()

    def parent(self, index):
        if not index.isValid():
            return QModelIndex()

        childItem = index.internalPointer()
        parentItem = childItem.parent()

        if parentItem == self.rootItem:
            return QModelIndex()

        return self.createIndex(parentItem.row(), 0, parentItem)

    def columnCount(self, parent=None, *args, **kwargs):
        if parent.isValid():
            return parent.internalPointer().columnCount()
        else:
            return self.rootItem.columnCount()

    def loadTree(self, filePath, nodedef):
        lst_unknown = []
        self.beginResetModel()
        self.rootItem.loadJsonFile(filePath, nodedef, lst_unknown)
        self.endResetModel()
        self.isLoaded = True
        return lst_unknown

    def getItem(self, index):
        if index.isValid():
            item = index.internalPointer()
            if item:
                return item

        return self.rootItem

    def removeRow(self, row, parent=QModelIndex()):
        parentItem = self.getItem(parent)
        self.beginRemoveRows(parent, row, row+1)
        ret = parentItem.removeChild(row)
        self.endRemoveRows()
        return ret

    def insertNewNode(self, row, type, nodeName, nodeType, parent):
        parentItem = self.getItem(parent)
        self.beginInsertRows(parent, row, row)
        ret = parentItem.insertChild(nodeName, nodeType, type, row)
        self.endInsertRows()
        return ret

    def moveItemPosition(self, parent, index, newposix):
        parentItem = self.getItem(parent)
        self.beginMoveRows(parent, index.row(), index.row(), parent, newposix)
        item = parentItem.popChild(index.row())
        parentItem.insertItem(item, newposix)
        self.endMoveRows()

    def setData(self, index, value, role=None):
        if not index.isValid():
            return False
        try:
            item = self.getItem(index)
            if item != self.rootItem:
                if index.column() == 0:
                    item.Name = value
                    return True
            return False
        except ValueError:
            return False

    def data(self, index, role):
        if not index.isValid():
            return None

        if role != Qt.DisplayRole and role != Qt.EditRole:
            return None

        item = self.getItem(index)
        if index.column() == 0:
            return item.Name
        else:
            return item.TypeName