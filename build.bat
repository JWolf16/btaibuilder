cd UI
call Make_Python_Ui.bat
cd ..
del BattleTechAiBuilder.exe
rmdir /Q /S dist
py -3 -m PyInstaller btaibuilder.spec
rmdir /Q /S build
copy /B /Y dist\BattleTechAiBuilder.exe .