from .behaviournodeitem import BehaviourTreeNode

class TreeLookupItem(object):

    def __init__(self, tree):
        """

        :param role:
        :type role:
        :param tree:
        :type tree: BehaviourTreeNode
        """

        self.roleList = []
        self.treeItem = tree

    def addRole(self, role):
        self.roleList.append(role)

    def hasRole(self, role):
        return role in self.roleList

    @property
    def treeName(self):
        return self.treeItem.Name
